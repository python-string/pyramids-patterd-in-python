# pattern Printing
n = int(input('Enter a number : '))
p_type = int(input('Enter 0 to Pyramid type and 1 for reverse Pyramid : '))
if p_type==0:
    for i in range(1,n+1):
        print((n-i) * ' ',end='')
        print(i * '* ')
else :
    for i in range(n,0,-1):
        print((n-i) * " ",end='')
        print(i * '* ')